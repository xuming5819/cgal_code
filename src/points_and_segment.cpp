//
// Created by xuming on 2022/8/13.
//

#include <iostream>
#include <CGAL/Simple_cartesian.h>


/*
 * Point_2 一个 2d 点
 * Segment 2d 的凸包，若只有两点则是线段
 * CGAL::squared_distance liangdian 两点的距离或点到线的距离
 * CGAL::orientation(p, q, m)`:三点是否共线，若不共线，则 m 在 p，q 哪一侧
 * CGAL::midpoint(p, q)`:中点坐标
 */

typedef CGAL::Simple_cartesian<double> Kernel;
typedef Kernel::Point_2 Point_2;
typedef Kernel::Segment_2 Segment_2;

int main()
{
    Point_2 p(1,1), q(10,10);       // Ponit_2 创建点
    std::cout << "p = " << p << std::endl;
    std::cout << "q = " << q.x() << " " << q.y() << std::endl;
    std::cout << "sqdist(p,q) = "
              << CGAL::squared_distance(p,q)    // 计算两点的距离
              << std::endl;
    Segment_2 s(p,q);                           // 线段
    Point_2 m(5, 9);
    std::cout << "m = " << m << std::endl;
    std::cout << "sqdist(Segment_2(p,q), m) = "
              << CGAL::squared_distance(s,m) << std::endl;      // 计算点到线的距离
    std::cout << "p, q, and m ";
     // 判断 p, q, m 三点是否共线, 判断点 m 在 p, q 哪一侧
    switch (CGAL::orientation(p,q,m)){
        case CGAL::COLLINEAR:
            std::cout << "are collinear\n";
            break;
        case CGAL::LEFT_TURN:
            std::cout << "make a left turn\n";
            break;
        case CGAL::RIGHT_TURN:
            std::cout << "make a right turn\n";
            break;
    }
    std::cout << " midpoint(p,q) = " << CGAL::midpoint(p,q) << std::endl;
    return 0;
}